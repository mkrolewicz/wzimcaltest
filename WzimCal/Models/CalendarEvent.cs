using System;
using System.Collections.Generic;

namespace WzimCal.Models
{
    public partial class CalendarEvent : IEntity
    {
        public int Id { get; set; }
        public DateTime CalendarEventDateTimeStart { get; set; }
        public DateTime CalendarEventDateTimeEnd { get; set; }
        public string Topic { get; set; }
        public string Comment { get; set; }
        public virtual ICollection<ApplicationUser> UsersAttending { get;set; }
        public virtual Location Location { get; set; }
    }
}
