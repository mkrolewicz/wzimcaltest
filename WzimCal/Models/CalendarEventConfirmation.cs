using System;
using System.Collections.Generic;

namespace WzimCal.Models
{
    public partial class CalendarEventConfirmation : IEntity
    {
        public int Id { get; set; }
        
        public bool IsConfirmed { get; set; }
        public string Comments { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public DateTime ConfirmationDateTime { get; set; }
    }
}
