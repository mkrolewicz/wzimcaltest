using System;
using System.Collections.Generic;

namespace WzimCal.Models
{
    public class Location : IEntity
    {
        public Location()
        {
            this.CalendarEvents = new List<CalendarEvent>();
        }
        
        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string BuildingNumber { get; set; }
        public int ApartmentNumber { get; set; }
        public virtual ICollection<CalendarEvent> CalendarEvents { get; set; }
    }
}
